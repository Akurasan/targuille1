#I should add to script 
#for i in {1..10}; do "filename$i"; done
#but not sure how exactly do it

#!/bin/bash

>test.txt
file_name=test.txt

current_time=$(date "+%Y.%m.%d-%H.%M.%S")
echo "Current Time : $current_time"

new_fileName=$file_name.$current_time
echo "New FileName: " "$new_fileName"

cp $file_name $new_fileName

openssl rand -base64 1000 >> $new_fileName

#I will schedule sript with help of cron utility (crontab -e). schedule task should be looking something like this: 0 6 * * * /absolute/path/to/filecreator.sh